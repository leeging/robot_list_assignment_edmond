# A&K Robot List - Edmond Lee

A web project created with React, Redux, and React-Router-Dom.

## Getting Started

- Install all dependencies using `npm install` command.
- Run the development web server using `npm start` command.
- The server should be hosted on http://localhost:3000.

Any feedback is appreciated.
