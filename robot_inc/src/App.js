import React, { Component } from 'react';
import './App.css'
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Home from './components/Home'

class App extends Component {
  render() {
    return (
        <Router>
          <div className="App">
            <Route exact path="/" component={Home} />
            <Route path="/status/online" component={Home} />
            <Route path="/status/offline" component={Home} />
            <Route path="/robot/details" component={Home} />
          </div>
        </Router>
    );
  }
}

export default App;
