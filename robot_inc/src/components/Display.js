import React, { Component } from 'react';
import Table from './Table'
import store from '../store'
import { Redirect } from 'react-router'
import Details from './Details'

export default class Display extends Component {
  constructor(props) {
  super(props);
    this.state = {
      redirect:false
    }
    this.checkStatus = this.checkStatus.bind(this)
  };

  // Checks for the status the user has chosen. If chosen, display the proper robots based on status.
  checkStatus() {
    let update;
    if (store.getState().displayStatus === 'off') {
      update = {
        type:'changeStatus',
        payload: {
          displayStatus: 'online'
        }
      }
    } else if (store.getState().displayStatus === 'online') {
      update = {
        type:'changeStatus',
        payload: {
          displayStatus: 'offline'
        }
      }
    } else {
      update = {
        type:'changeStatus',
        payload: {
          displayStatus: 'off'
        }
      }
    }
    store.dispatch(update)
    this.setState({redirect:true})
  }

  render() {
    const status = store.getState().displayStatus
    const { redirect } = this.state;
    if (status === 'off' && redirect) {
      return <Redirect to='/'/>;
    } else if (status === 'online' && redirect) {
      return <Redirect to='/status/online'/>
    } else if (status === 'offline' && redirect) {
      return <Redirect to='/status/offline'/>
    };
    
    // If specific robot has been chosen, render details, otherwise render table.
    let display;
    if (store.getState().robotDetails !== '') {
      display = 
      <div>
        <div className="screen-header">
          <table className="list-header"> 
            <tbody>
              <tr>
                <th>ROBOT DETAILS</th> 
              </tr>
            </tbody>
          </table> 
        </div>
        <div className="screen">
          <Details/>
        </div>
      </div>
    } else {
      display = 
      <div>
        <div className="screen-header">
          <table className="list-header"> 
            <tbody>
              <tr>
                <th>Robot</th>
                <th>ID</th> 
                <th onClick={this.checkStatus}>Status</th>
              </tr>
            </tbody>
          </table> 
        </div>
        <div className="screen">
          <Table/>
        </div>
      </div>
    }
    return (
      <div>
        {display}
      </div>
    )
  }
}
