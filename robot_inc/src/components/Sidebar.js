import React, { Component } from 'react';
import store from '../store'
import { Redirect } from 'react-router'

export default class List extends Component {
  constructor(props) {
    super(props);
    this.state = {
      redirect:false,
      redirect2:false,
      helpEnabled: false,
    }
    this.redirectHome = this.redirectHome.bind(this)
    this.helpEnabled = this.helpEnabled.bind(this)
  };

  // Toggles the instruction displays.
  helpEnabled() {
    this.setState({helpEnabled: !this.state.helpEnabled})
    console.log(this.state.helpEnabled)
  }

  redirectHome(){
    const update = {
      type:'changeRobot',
      payload: {
        robotDetails: ''
      }
    }
    store.dispatch(update)
    this.setState({redirect: true});
    window.location.reload();
  }

  render() {
    const { redirect } = this.state;
    const { helpEnabled } = this.state;
    let instructions;
    if (redirect) {
      return <Redirect to='/'/>
    }
    if (helpEnabled) {
      instructions = 
      <div>
        <p>Welcome to the ROBOT INC DASHBOARD</p>
        <p> The home page provides a list of all of the robots, along with their names, their IDs and their statuses. Click on any row to view or edit the robot's details.</p>
        <p> To filter the robots by status, click the status header at the top of the display. </p>
        <p> To return home, click the home button on left sidebar. </p>
      </div>
    } else {
      instructions = <p> </p>
    }
    return (
      <div className="sidebar">
        <div className="sidebar-container">
          <h3>ROBOT INC <br/> DASHBOARD</h3>
          <button onClick={this.redirectHome}> HOME </button>
          <button onClick={this.helpEnabled}> HELP </button>
          {instructions}
        </div>
      </div>
    )
  }
}
