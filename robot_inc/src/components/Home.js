import Display from './Display'
import React, { Component } from 'react';
import Sidebar from './Sidebar'


export default class Home extends Component {
  render() {
    return (
      <div>
        <Sidebar/>
        <h1> ROBOT INC. </h1>
        <Display/>
      </div>
    )
  }
}
