import React, { Component } from 'react';
import { Redirect } from 'react-router'
import store from '../store'

export default class Table extends Component {
  constructor(props) {
    super(props);
    this.state = {
      redirect: false,
      attributes: ''
    }
    this.updateRobot = this.updateRobot.bind(this)
  }

  // Converts the robot attributes object into a string, allowing for simple user edit. 
  accessAttributes() {
    if (this.state.attributes === '') {
      const robot = store.getState().robotDetails
      const keyArray = [];
      const valueArray = [];
      let str = '';
      Object.keys(robot.attributes).forEach(function(key) {
        keyArray.push(key)
        valueArray.push(robot.attributes[key])
        });
      keyArray.forEach(function (key, index) {
        str += (key + ": " + valueArray[index] + ", ")
      });
      this.setState({attributes: str})
    };
  }

  // Converts the user edit string into an object, and performs a PUT method to update specified robot attributes.
  updateRobot() {
    const arr = this.state.attributes.replace(/\s/g,'').split(',')
    const arr2 = []
    arr.forEach(function(key) {
      let temp = key.split(':')
      arr2.push(temp)
    })
    const obj = Object.assign(...arr2.map(d => ({[d[0]]: d[1]})))
    delete obj["customerId"]
    delete obj["robotId"]
    const updateAPI = 'https://6i9nu7ctv0.execute-api.us-west-2.amazonaws.com/test/robots/name/' + store.getState().robotDetails.thingName
    try {
      fetch( updateAPI, {
        method: 'PUT',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          attributes : obj
        })
      })
    } catch (err) {
      console.log(err)
    }
  }

  onTodoChange(value){
    this.setState({
      attributes: value
    });
  }

  componentDidMount() {
    this.accessAttributes()
  }

  render() {
    const attributes = this.state.attributes
    const robot = store.getState().robotDetails
    const { redirect } = this.state;
    if (redirect) {
      return <Redirect to='/robot/details'/>
    }
    return (
      <div>
        <div className="robot-details">
          <h4>Thing Name</h4>
          <p>{robot.thingName}</p>
          <h4> Thing ID </h4>
          <p>{robot.thingId}</p>
          <h4>Attributes</h4>
          <p>{attributes}</p>
          <h4>Version</h4>
          <p>{robot.version}</p>
          <h4>Status</h4>
          <p>{robot.status}</p>
          <label>
            <h4>Edit Attributes:</h4>
            <p className="edit-instructions"> When editing the attributes, please follow the syntax. Please sepearete each pair with a comma. <br/> EXAMPLE: key:value</p>
            <input className="edit-input" type="text" value = { attributes } onChange={e => this.onTodoChange(e.target.value)}/>
          </label>
          <button onClick={this.updateRobot} className="save-button" type="button">SAVE</button>
        </div>
      </div>
    )
  }
}
