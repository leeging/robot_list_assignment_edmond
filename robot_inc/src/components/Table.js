import React, { Component } from 'react';
import store from '../store'
import { Redirect } from 'react-router'

export default class Table extends Component {
  constructor(props) {
    super(props);
    this.state = {
      allRobots: [],
      onlineRobots: [],
      offlineRobots: [],
      displayedRobots: [],
      redirect: false,
    }
    this.sortRobots = this.sortRobots.bind(this)
    this.viewRobot = this.viewRobot.bind(this)
  }

  // Retrieves specified robot details and stores for details.
  viewRobot(robot) {
    let update = {
        type:'changeRobot',
        payload: {
          robotDetails: robot
        }
      }
    store.dispatch(update)
    this.setState({redirect:true})
  };

  // Sorts robots depending on status.
  sortRobots() {
    let onlineRobots = [];
    let offlineRobots = [];
    this.state.allRobots.forEach(function (robot) {
      if (robot.status === 'online') {
        onlineRobots.push(robot)
      } else {
        offlineRobots.push(robot)
      }
    });
    this.setState({displayedRobots: this.state.allRobots});
    this.setState({onlineRobots: onlineRobots});
    this.setState({offlineRobots: offlineRobots});
    const displayStatus = store.getState().displayStatus.toString();
    if (displayStatus ==='online' && this.state.displayedRobots !== this.state.onlineRobots) {
      this.setState({displayedRobots: this.state.onlineRobots})
    } else if (displayStatus ==='offline' && this.state.displayedRobots !== this.state.offlineRobots) {
      this.setState({displayedRobots: this.state.offlineRobots})
    }
  };

  // GET method to fetch list of robots.
  componentDidMount() {
    fetch('https://6i9nu7ctv0.execute-api.us-west-2.amazonaws.com/test/robots')
      .then(res => res.json())
      .then(data => this.setState({allRobots: data}))
      .then(this.setState({displayedRobots: this.state.allRobots}))
      .then(this.sortRobots)
  };

  render() {
    const { redirect } = this.state;
    if (redirect) {
      return <Redirect to='/robot/details'/>
    }
    const viewRobot = this.viewRobot
    return (
      <div>
        <table className="list-table">
          <tbody>
            {this.state.displayedRobots.map(function(robot, key) {
              return (
                <tr onClick={viewRobot.bind(this, robot)} key={key}>
                  <td>{robot.thingName}</td>
                  <td>{robot.thingId}</td>
                  <td>{robot.status}</td>
                </tr>
              )
            })}
          </tbody>
        </table>
        <h1 className="loading"> LOADING... </h1>
      </div>
    )
  }
}
