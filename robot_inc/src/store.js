import { createStore, combineReducers } from 'redux'; 

function statusReducer(state = [], action) {
  if (action.type === 'changeStatus') {
    return action.payload.displayStatus
  }
  return state;
}

function robotReducer(state = '', action) {
  if (action.type === 'changeRobot') {
    return action.payload.robotDetails
  }
  return state;
}

const allReducers = combineReducers({
  displayStatus: statusReducer,
  robotDetails: robotReducer
})

const store = createStore(
  allReducers,
{
  displayStatus: 'off',
  robotDetails: ''
},

window.devToolsExtension && window.devToolsExtension()
);

export default store;
